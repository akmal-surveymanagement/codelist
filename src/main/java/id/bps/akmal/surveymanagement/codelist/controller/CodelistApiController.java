package id.bps.akmal.surveymanagement.codelist.controller;

import id.bps.akmal.surveymanagement.codelist.dao.CodelistDao;
import id.bps.akmal.surveymanagement.codelist.entity.Codelist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController @RequestMapping("/api/codelist")
public class CodelistApiController {
    @Autowired private CodelistDao codelistDao;

    @GetMapping(
            value = "/list"
    )
    public Page<Codelist> findCodelists(Pageable page){
        return codelistDao.findAll(page);
    }

    @GetMapping(
        value = "/",
            params = {"c_id"}
    )
    public Page<Codelist> findCodelists(@RequestParam("c_id") String codelistid, Pageable page){
        return codelistDao.findByCodelistIdContainingIgnoreCase(codelistid, page);
    }

//    @RequestMapping(value="user", method = RequestMethod.GET)
//    public @ResponseBody item getitem(@RequestParam("data") String itemid){
//
//        item i = itemDao.findOne(itemid);
//        String Itemname=i.getItemname();
//        String price= i.getPrice();
//        return i;
//    }

    @GetMapping("/{id}")
    public Codelist findById(@PathVariable("id") Codelist c){
        return c;
    }
}
