package id.bps.akmal.surveymanagement.codelist.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
public class Codelist {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Getter @Setter
    private String id;


    @NotNull @NotEmpty
    @Getter @Setter
    @Column(name = "codelist_id")
    private String codelistId;

    @NotNull @NotEmpty
    @Getter @Setter
    private int code;

    @Getter @Setter
    private String isi;

    @Getter @Setter
    private String creator;


    @DateTimeFormat(pattern = "dd-mm-yyyy hh24:mi:ss")
    @Getter @Setter
    @Column(name = "create_date")
    private Date createDate;

    @Getter @Setter
    private String approval;

    @DateTimeFormat(pattern = "dd-mm-yyyy hh24:mi:ss")
    @Getter @Setter
    @Column(name = "approval_date")
    private Date approvalDate;

    @Getter @Setter
    private String flag;


    @Getter @Setter
    @Column(name = "last_modifier")
    private String lastModifier;

    @DateTimeFormat(pattern = "dd-mm-yyyy hh24:mi:ss")
    @Getter @Setter
    @Column(name = "modify_date")
    private Date modifyDate;
}
