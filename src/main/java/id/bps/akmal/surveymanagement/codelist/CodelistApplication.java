package id.bps.akmal.surveymanagement.codelist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication @EnableDiscoveryClient
public class CodelistApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodelistApplication.class, args);
	}
}
