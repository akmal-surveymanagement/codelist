package id.bps.akmal.surveymanagement.codelist.dao;

import id.bps.akmal.surveymanagement.codelist.entity.Codelist;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CodelistDao extends PagingAndSortingRepository<Codelist, String> {
    public Page<Codelist> findByCodelistIdContainingIgnoreCase(String codelistId, Pageable pageable);
}
