insert into codelist(id, codelist_id, code, isi, creator, approval, flag, last_modifier, create_date, approval_date, modify_date) VALUES
('p003', 'ijazah',	1,	'Tidak Punya Ijazah SD',	'sosial',	'metodologi', 'A', 'metodologi', '2018-09-13 8:18:00', '2018-09-13 20:18:00', '2018-09-14 8:18:00'),
('p004', 'ijazah',	2,	'Paket A',	'sosial',	'metodologi', 'A', 'metodologi', '2018-09-14 8:18:00', '2018-09-14 20:18:00', '2018-09-15 8:18:00'),
('p005', 'ijazah',	3,	'SDLB',	'sosial',	'metodologi', 'A', 'metodologi', '2018-09-15 8:18:00', '2018-09-15 20:18:00', '2018-09-16 8:18:00'),
('p006', 'ijazah',	4,	'SD/MI',	'sosial',	'metodologi', 'A', 'metodologi', '2018-09-16 8:18:00', '2018-09-16 20:18:00', '2018-09-17 8:18:00'),
('p007', 'ijazah',	5,	'Paket B',	'sosial',	'metodologi', 'A', 'metodologi', '2018-09-17 8:18:00', '2018-09-17 20:18:00', '2018-09-18 8:18:00'),
('p008', 'ijazah',	6,	'SMPLB',	'sosial',	'metodologi', 'A', 'metodologi', '2018-09-18 8:18:00', '2018-09-18 20:18:00', '2018-09-19 8:18:00'),
('p009', 'ijazah',	7,	'SMP/MTS',	'sosial',	'metodologi', 'A', 'metodologi', '2018-09-19 8:18:00', '2018-09-19 20:18:00', '2018-09-20 8:18:00'),
('p010', 'ijazah',	8,	'Paket C',	'sosial',	'metodologi', 'A', 'metodologi', '2018-09-20 8:18:00', '2018-09-20 20:18:00', '2018-09-21 8:18:00'),
('p011', 'ijazah',	9,	'SMALB',	'sosial',	'metodologi', 'A', 'metodologi', '2018-09-21 8:18:00', '2018-09-21 20:18:00', '2018-09-22 8:18:00'),
('p012', 'ijazah',	10,	'SMA/MA',	'sosial',	'metodologi', 'A', 'metodologi', '2018-09-22 8:18:00', '2018-09-22 20:18:00', '2018-09-23 8:18:00'),
('p013', 'ijazah',	11,	'SMK',	'sosial',	'metodologi', 'A', 'metodologi', '2018-09-23 8:18:00', '2018-09-23 20:18:00', '2018-09-24 8:18:00'),
('p014', 'ijazah',	12,	'Diploma I/II',	'sosial',	'metodologi', 'A', 'metodologi', '2018-09-24 8:18:00', '2018-09-24 20:18:00', '2018-09-25 8:18:00'),
('p015', 'ijazah',	13,	'Diploma III',	'sosial',	'metodologi', 'A', 'metodologi', '2018-09-25 8:18:00', '2018-09-25 20:18:00', '2018-09-26 8:18:00'),
('p016', 'ijazah',	14,	'D-IV/S1',	'sosial',	'metodologi', 'A', 'metodologi', '2018-09-26 8:18:00', '2018-09-26 20:18:00', '2018-09-27 8:18:00'),
('p017', 'ijazah',	15,	'S2',	'sosial',	'metodologi', 'A', 'metodologi', '2018-09-27 8:18:00', '2018-09-27 20:18:00', '2018-09-28 8:18:00'),
('p018', 'ijazah',	16,	'S3',	'sosial',	'metodologi', 'A', 'metodologi', '2018-09-28 8:18:00', '2018-09-28 20:18:00', '2018-09-29 8:18:00'),

('p019', 'klasifikasidesa',	1,	'Pedesaan',	'sosial',	'metodologi', 'A', 'metodologi', '2018-09-29 8:18:00', '2018-09-29 20:18:00', '2018-09-30 8:18:00'),
('p020', 'klasifikasidesa',	2,	'Perkotaan', 'sosial',	'metodologi', 'A', 'metodologi', '2018-09-30 8:18:00', '2018-09-30 20:18:00', '2018-10-01 8:18:00'),

('p021', 'yatidak',	1,	'Ya', 'sosial',	'metodologi', 'A', 'metodologi', '2018-10-01 8:18:00', '2018-10-01 20:18:00', '2018-10-02 8:18:00'),
('p022', 'yatidak',	2,	'Tidak', 'sosial',	'metodologi', 'A', 'metodologi', '2018-10-02 8:18:00', '2018-10-02 20:18:00', '2018-10-03 8:18:00');