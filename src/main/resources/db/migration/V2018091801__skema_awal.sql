create table codelist(
    id varchar(255),
    codelist_id varchar(200) NOT NULL,
    code int NOT NULL,
    isi varchar(200),
    creator varchar(70),
    create_date TIMESTAMP,
    approval varchar(70),
    approval_date TIMESTAMP,
    flag varchar(2),
    last_modifier varchar(70),
    modify_date TIMESTAMP
);

ALTER TABLE ONLY codelist
    ADD CONSTRAINT codelist_pkey PRIMARY KEY (id);